## PDFmake
* Es una libreria en javascript que permite generar archivos PDF tanto para cliente y como servidor para *NodeJS* (para más información de clic [aquí](https://pdfmake.github.io/docs/)).

## Creación del proyecto 
* Lo primero a realizar será crear un nuevo proyecto de *NodeJS* bajo la plataforma *NestJS*, para ello será necesario abrir una terminal e ingresar el siguiente comando: 

```javascript
nest new ejemplo-pdf-make
```

## Instalación
* Para instalar la librería *PDFmake* , desde la terminal debemos ubicarnos en el directorio de nuestro proyecto creado e ingresar el siguiente comando.

```javascript
npm install pdfmake
```

## Configuración
* Para crear un repositorio de fuentes de letras estándar debe ejecutar los siguientes comandos:

```javascript
git clone https://github.com/bpampuch/pdfmake.git
cd pdfmake
npm install
gulp buildWithStandardFonts
```

## Creación del archivo

* Ubicarse dentro de la carpeta **src** y con su *IDE ( Integrated Development Environment)* abrir el archivo **app.service.ts**, donde deberá copiar las siguientes funciones: 

```javascript
async crearFactura(): Promise<{ errorCrudo?: string, error?: number, mensaje: string }> {
    return new Promise(
      async (resolve, reject) => {
          //Configuración de la fuente
        const configuracionFuente = {
          Roboto: {
            normal: __dirname + '/fonts/Roboto-Regular.ttf',
            bold: __dirname + 'fonts/Roboto-Medium.ttf',
            italics: __dirname + 'fonts/Roboto-Italic.ttf',
            bolditalics: __dirname + 'fonts/Roboto-MediumItalic.ttf',
          },
        }
        const printer = new pdfMakePrinter(configuracionFuente);

        try {
            //Creación del archivo pdf
          const nombreArchivo = 'ejemploPDFmaker'
          const pdfDoc = printer.createPdfKitDocument(
            this.crearDefinicionDocumentoPDF(),
          );

          // Almacenamiento del archivo en su equipo
          pdfDoc
            .pipe(
                //Definición de la ruta destino del archibo
              fs.createWriteStream(
               nombreArchivo + '.pdf'
              ),
            )
            .on('finish', () => {
              resolve(
                {
                  mensaje: 'Archivo creado correctamente'
                }
              );
            });
          pdfDoc.end();
        } catch (e) {
          console.error('Error creando pdf:', e);
          resolve(
            {
              error: 500,
              mensaje: 'Error creando pdf',
              errorCrudo: e
            }
          );
        }
      });
  }
```


```javascript
 crearDefinicionDocumentoPDF() {
     //Definición del contenido del archivo PDF
    return {
        //Creación de un encabezado que muestra el número de la página actual.
      footer(currentPage, pageCount) {
        return currentPage.toString() + ' de ' + pageCount;
      },
      content: [
          //Creación de una tabla de ejemplo
        {text: 'Tables', style: 'header'},
        'Official documentation is in progress, this document is just a glimpse of what is possible with pdfmake and its layout engine.',
        {text: 'A simple table (no headers, no width specified, no spans, no styling)', style: 'subheader'},
        'The following table has nothing more than a body array',
        {
          style: 'tableExample',
          table: {
            body: [
              ['Column 1', 'Column 2', 'Column 3'],
              ['One value goes here', 'Another one here', 'OK?']
            ]
          }
        },
      ]
    };
  }

```

* En el archivo **app.controller.ts**, debe crear un nuevo método que permita ejecutar las funciones creadas anteriormente desde el navegador.

```javascript
  @Get('crear-pdf')
  crearPDF() {
    return this.appService.crearFactura();
  }
```


## Resultados

* Desde nuestro navegador accedemos a al siguiente enlace:
```
http://localhost:3000/crear-pdf
```

* Se debe mostrar el siguiente mensaje, el cual indica que el archivo se ha creado correctamente.

```
{
"mensaje":"Archivo creado correctamente"
}
```

* Para comprobar que el archivo se ha creado, debe dirigirse al directorio especificado anteriormente, en este caso será **src** y se abrir el archivo creado.

![alt text](./imagenes/archivo-pdf-creado.png)
