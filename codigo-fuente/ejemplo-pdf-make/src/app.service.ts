import * as pdfMakePrinter from 'pdfmake/src/printer';
import * as fs from 'fs';
import { Injectable } from '@nestjs/common';
@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }


  async crearFactura(): Promise<{ errorCrudo?: string, error?: number, mensaje: string }> {
    return new Promise(
      async (resolve, reject) => {
        const configuracionFuente = {
          Roboto: {
            normal: __dirname + '/fonts/Roboto-Regular.ttf',
            bold: __dirname + 'fonts/Roboto-Medium.ttf',
            italics: __dirname + 'fonts/Roboto-Italic.ttf',
            bolditalics: __dirname + 'fonts/Roboto-MediumItalic.ttf',
          },
        }
        const printer = new pdfMakePrinter(configuracionFuente);
        const nombreArchivo = 'ejemploPDFmaker'
        try {
          const pdfDoc = printer.createPdfKitDocument(
            this.crearDefinicionDocumentoPDF(),
          );
          pdfDoc
            .pipe(
              fs.createWriteStream(
               nombreArchivo + '.pdf'
              ),
            )
            .on('finish', () => {
              resolve(
                {
                  mensaje: 'Archivo creado correctamente'
                }
              );
            });
          pdfDoc.end();
        } catch (e) {
          console.error('Error creando pdf:', e);
          resolve(
            {
              error: 500,
              mensaje: 'Error creando pdf',
              errorCrudo: e
            }
          );
        }
      });

  }

  crearDefinicionDocumentoPDF() {
    return {
      footer(currentPage, pageCount) {
        return currentPage.toString() + ' de ' + pageCount;
      },
      content: [
        {text: 'Tables', style: 'header'},
        'Official documentation is in progress, this document is just a glimpse of what is possible with pdfmake and its layout engine.',
        {text: 'A simple table (no headers, no width specified, no spans, no styling)', style: 'subheader'},
        'The following table has nothing more than a body array',
        {
          style: 'tableExample',
          table: {
            body: [
              ['Column 1', 'Column 2', 'Column 3'],
              ['One value goes here', 'Another one here', 'OK?']
            ]
          }
        },
      ]
    };
  }


}
