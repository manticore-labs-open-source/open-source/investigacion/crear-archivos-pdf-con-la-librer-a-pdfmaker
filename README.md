## PDFmake


* Es una libreria en javascript que permite generar archivos PDF tanto para cliente y como servidor para *NodeJS* .

* Permite crear e imprimir archivos PDF directamente en el navegador o desde el backend en *NODE.js*.

* Este proyecto contiene un ejemplo básico de como instlar esta librería, configurar las fuentes y creación de un documento pdf. Donde se explica como definir párrafos, columnas, listas, tablas. crear propios estilos y  fuentes personalizadas.

![alt text](./codigo-fuente/imagenes/node-pdf.jpg)
