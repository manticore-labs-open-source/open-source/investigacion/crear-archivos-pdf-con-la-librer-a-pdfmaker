"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const pdfMakePrinter = require("pdfmake/src/printer");
const fs = require("fs");
const common_1 = require("@nestjs/common");
let AppService = class AppService {
    getHello() {
        return 'Hello World!';
    }
    async crearFactura() {
        return new Promise(async (resolve, reject) => {
            const configuracionFuente = {
                Roboto: {
                    normal: __dirname + '/fonts/Roboto-Regular.ttf',
                    bold: __dirname + 'fonts/Roboto-Medium.ttf',
                    italics: __dirname + 'fonts/Roboto-Italic.ttf',
                    bolditalics: __dirname + 'fonts/Roboto-MediumItalic.ttf',
                },
            };
            const printer = new pdfMakePrinter(configuracionFuente);
            const nombreArchivo = 'ejemploPDFmaker';
            try {
                const pdfDoc = printer.createPdfKitDocument(this.crearDefinicionDocumentoPDF());
                pdfDoc
                    .pipe(fs.createWriteStream(nombreArchivo + '.pdf'))
                    .on('finish', () => {
                    resolve({
                        mensaje: 'Archivo creado correctamente'
                    });
                });
                pdfDoc.end();
            }
            catch (e) {
                console.error('Error creando pdf:', e);
                resolve({
                    error: 500,
                    mensaje: 'Error creando pdf',
                    errorCrudo: e
                });
            }
        });
    }
    crearDefinicionDocumentoPDF() {
        return {
            footer(currentPage, pageCount) {
                return currentPage.toString() + ' de ' + pageCount;
            },
            content: [
                { text: 'Tables', style: 'header' },
                'Official documentation is in progress, this document is just a glimpse of what is possible with pdfmake and its layout engine.',
                { text: 'A simple table (no headers, no width specified, no spans, no styling)', style: 'subheader' },
                'The following table has nothing more than a body array',
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            ['Column 1', 'Column 2', 'Column 3'],
                            ['One value goes here', 'Another one here', 'OK?']
                        ]
                    }
                },
            ]
        };
    }
};
AppService = __decorate([
    common_1.Injectable()
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map