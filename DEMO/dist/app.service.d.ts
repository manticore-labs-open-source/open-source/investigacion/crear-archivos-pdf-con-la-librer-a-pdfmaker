export declare class AppService {
    getHello(): string;
    crearFactura(): Promise<{
        errorCrudo?: string;
        error?: number;
        mensaje: string;
    }>;
    crearDefinicionDocumentoPDF(): {
        footer(currentPage: any, pageCount: any): string;
        content: (string | {
            text: string;
            style: string;
            table?: undefined;
        } | {
            style: string;
            table: {
                body: string[][];
            };
            text?: undefined;
        })[];
    };
}
