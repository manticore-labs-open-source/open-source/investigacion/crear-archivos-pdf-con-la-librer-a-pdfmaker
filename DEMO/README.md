## Requisitos
* Node,js 12.16.1
* PDFmake version 0.1 (revisar el siguente [archivo](./../codigo-fuente/README.md) para su instalación)


## Ejecución del proyecto
* Desde una consola cambiarse al directorio **dist** y ejecutar el siguiente comando:

```javascript
node main.js
```

* La consola debe mostrar los siguientes logs.

![alt text](./../codigo-fuente/imagenes/node-mainjs.png)

## Resultados

* Desde nuestro navegador accedemos a al siguiente enlace:
```
http://localhost:3000/crear-pdf
```

* Se debe mostrar el siguiente mensaje, el cual indica que el archivo se ha creado correctamente.

```
{
"mensaje":"Archivo creado correctamente"
}
```

* Para comprobar que el archivo se ha creado, debe dirigirse al directorio especificado anteriormente, en este caso será **dist** y se abrir el archivo pdf creado.

![alt text](./../codigo-fuente/imagenes/archivo-pdf-creado.png)
